import 'dart:convert';

import 'package:covid_control/domain/login/model/login_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppStorage {
  static const String _tokenKey = 'token';
  static const String _loginDataKey = 'loginData';

  Future<String> getToken() async {
    final pref = await SharedPreferences.getInstance();
    return pref.get(_tokenKey) ?? '';
  }

  Future<void> setToken(String token) async {
    final pref = await SharedPreferences.getInstance();
    pref.setString(_tokenKey, token);
  }

  Future<void> setLoginData(LoginData data) async {
    final pref = await SharedPreferences.getInstance();
    pref.setString(_loginDataKey, jsonEncode(data.toMap()));
  }

  Future<LoginData> getLoginData() async {
    final pref = await SharedPreferences.getInstance();
    final stringJson = pref.get(_loginDataKey);
    if (stringJson == null) return null;
    return LoginData.fromMap(jsonDecode(stringJson));
  }
}