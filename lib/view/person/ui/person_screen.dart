import 'package:covid_control/app_constants.dart';
import 'package:covid_control/base/util.dart';
import 'package:covid_control/data/trip/trip_repository.dart';
import 'package:covid_control/domain/person/model/person.dart';
import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/trip.dart';
import 'package:covid_control/domain/trip/model/trip_data.dart';
import 'package:covid_control/domain/trip/model/work.dart';
import 'package:covid_control/view/base/style/colors.dart';
import 'package:covid_control/view/base/ui/widget/buttons.dart';
import 'package:covid_control/view/base/ui/widget/form_widgets.dart';
import 'package:covid_control/view/base/ui/widget/permissions_widget.dart';
import 'package:covid_control/view/person/widget/table_activity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:string_mask/string_mask.dart';

class PersonScreen extends StatefulWidget {
  final Person person;

  const PersonScreen({Key key, @required this.person}) : super(key: key);

  @override
  _PersonScreenState createState() => _PersonScreenState();
}

class _PersonScreenState extends State<PersonScreen> {
  TripRepository _tripRepository = TripRepository();
  Future<TripData> _future;

  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: FutureBuilder<TripData>(
          future: _future,
          builder: (context, state) {
            if (state.hasData) {
              return _buildData(state.data);
            }
            if (state.connectionState == ConnectionState.waiting) {
              return _buildProgress();
            }
            if (state.hasError) {
              return _buildError();
            }

            return SizedBox();
          },
        ),
      ),
    );
  }

  void _fetchData() {
    _future = _tripRepository.getTrips(widget.person.id);
    setState(() {});
  }

  Widget _buildProgress() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildError() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text('Ошибка загрузки данных'),
        SizedBox(height: 24),
        RaisedButton(
          child: Text('Обновить'),
          onPressed: _fetchData,
        ),
        SizedBox(height: 8),
        RaisedButton(
          child: Text('Назад'),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
  }

  Widget _buildIsSick(Quarantine quarantine, List<Work> works) {
    return PermissionsWidget(
      quarantine: quarantine,
      works: works,
    );
  }

  Widget _buildData(TripData data) {
    final trips = data.trips;
    var body = <Widget>[];

    if (trips.isNotEmpty) {
      final Trip firstTrip = trips.first;
      final List<Trip> tableTrips =
          trips.skipWhile((value) => value.timeStart == null).toList();

      body = <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: TextForm(
            title: 'Адрес пребывания',
            subTitle: firstTrip.pointStart ?? 'Не указан',
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: ViolationTextForm(
            title: 'Направляется',
            subTitle: firstTrip.pointEnd ?? 'Не указан',
            isViolation: firstTrip.isTimeViolation == true,
          ),
        ),
        SizedBox(height: 16),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: ViolationTextForm(
            title: 'Причина',
            subTitle: firstTrip.reason?.title,
            isViolation: firstTrip.isTimeViolation == true,
          ),
        ),
        SizedBox(height: 16),
        if (firstTrip.timeStart != null && firstTrip.timeStart.isNotEmpty)
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: TimeForm(dateStart: firstTrip.timeStart, isViolation: firstTrip.isTimeViolation ?? false,),
          ),
        SizedBox(height: 32),
        if (tableTrips.isNotEmpty)
          _buildTableActivity(tableTrips),
      ];
    }

    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: <Widget>[
              SizedBox(height: 32),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: PersonWidget(
                  name: widget.person.name.isNotEmpty
                      ? widget.person.name
                      : StringMask(AppConst.passportStringMask)
                          .apply(widget.person.passport),
                  passport: widget.person.passport,
                ),
              ),
              SizedBox(height: 16),
              _buildIsSick(data.quarantine, data.works),
              SizedBox(height: 16),
              if (trips.isEmpty)
                Padding(
                    padding: EdgeInsets.symmetric(horizontal: 32),
                    child: Text('Данных нет')),
              ...body,
            ],
          ),
        ),
        SizedBox(height: 16),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 32),
          width: double.infinity,
          child: AppAccentMainButton(
            text: 'Вернуться назад',
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        SizedBox(height: 16),
      ],
    );
  }

  Widget _buildTableActivity(List<Trip> trips) {
    final textStyle = TextStyle(
      fontSize: 12,
    );
    final textStyleSelected = textStyle.apply(color: Colors.white);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Журнал активности', style: Theme.of(context).textTheme.title,),
          SizedBox(height: 16),
          TableActivity(trips: trips)
        ],
      ),
    );
  }
}

class PersonWidget extends StatelessWidget {
  final String name;
  final String passport;

  const PersonWidget({Key key, this.name, this.passport}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          name,
          style: Theme.of(context)
              .textTheme
              .headline
              .copyWith(fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 4),
        Text(
          'Паспорт: $passport',
          style: Theme.of(context).textTheme.body2,
        ),
      ],
    );
  }
}
