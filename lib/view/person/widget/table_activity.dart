import 'package:covid_control/base/util.dart';
import 'package:covid_control/domain/trip/model/trip.dart';
import 'package:flutter/material.dart';

class TableActivity extends StatelessWidget {
  final List<Trip> trips;

  TableActivity({Key key, @required this.trips}) : super(key: key);


  final textStyle = TextStyle(
    fontSize: 12,
  );

  final textStyleSelected = TextStyle(
    fontSize: 12,
    color: Colors.white,
  );

  @override
  Widget build(BuildContext context) {

    return Container(
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'Время выхода',
                    style: textStyle.apply(color: Colors.black54),
                  ),
                ),
              ),
              Expanded(
                flex: 4,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Text(
                    'Цель',
                    style: textStyle.apply(color: Colors.black54),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 4),
          for (final trip in trips)
            Container(
              color: trip.isTimeViolation == true ? Colors.red : Colors.transparent,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            dateFormat(trip.timeStart, 'dd.MM.yy HH:mm') ?? 'Не указано',
                            style: trip.isTimeViolation == true ? textStyleSelected : textStyle,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          padding: EdgeInsets.all(8),
                          child: Text(
                            trip.reason?.title ??
                                trip.destinationName ??
                                'Не указано',
                            style: trip.isTimeViolation == true ? textStyleSelected : textStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    width: double.infinity,
                    height: 1,
                    color: Colors.black12,
                  )
                ],
              ),
            )
        ],
      ),
    );
  }
}
