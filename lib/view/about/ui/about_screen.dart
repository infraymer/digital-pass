import 'package:covid_control/app.dart';
import 'package:covid_control/base/config.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildTitle(context),
            SizedBox(height: 56),
            _buildVersion(context),
          ],
        ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Align(alignment: Alignment.centerLeft, child: BackButton()),
          Text('О приложении', style: Theme.of(context).textTheme.title),
        ],
      ),
    );
  }

  Widget _buildVersion(BuildContext context) {
    return FutureBuilder<PackageInfo>(
      future: PackageInfo.fromPlatform(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final info = snapshot.data;
          return Column(
            children: <Widget>[
              if (appConfig.buildParam == BuildParam.stage)
                Text('Тестовая сборка', style: TextStyle(color: Colors.red)),
              SizedBox(height: 8),
              Text('Версия',),
              SizedBox(height: 8),
              Text(
                info.version,
                style: Theme.of(context).textTheme.subtitle,
              ),
              SizedBox(height: 16),
              Text('Сборка'),
              SizedBox(height: 8),
              Text(
                info.buildNumber,
                style: Theme.of(context).textTheme.subtitle,
              ),
            ],
          );
        }

        return SizedBox();
      },
    );
  }
}
