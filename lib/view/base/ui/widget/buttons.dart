import 'package:covid_control/view/base/style/colors.dart';
import 'package:flutter/material.dart';

class AppAccentMainButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final Color color;
  final Color textColor;

  const AppAccentMainButton({Key key, this.text, this.onPressed, this.color, this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 0,
      splashColor: Colors.white24,
      color: color ?? Theme.of(context).accentColor,
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Text(text, style: TextStyle(fontSize: 14, color: textColor ?? Colors.white)),
      onPressed: onPressed,
    );
  }
}

class AppMainButton extends StatelessWidget {
  final String text;
  final Function onPressed;

  const AppMainButton({Key key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      elevation: 0,
      splashColor: Colors.white24,
      color: AppColors.button,
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Text(text,
          style: TextStyle(fontSize: 14, color: Colors.black)),
      onPressed: onPressed,
    );
  }
}
