import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/work.dart';
import 'package:covid_control/view/base/ui/widget/status_person_widget.dart';
import 'package:flutter/material.dart';

class PermissionsWidget extends StatelessWidget {
  final Quarantine quarantine;
  final List<Work> works;

  const PermissionsWidget({Key key, this.quarantine, this.works})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildIsSick(quarantine, works);
  }

  Widget _buildIsSick(Quarantine quarantine, List<Work> works) {
    return quarantine != null
        ? BadStatusPersonWidget(
           quarantine: quarantine,
          )
        : GoodStatusPersonWidget(
            works: works,
          );
  }
}
