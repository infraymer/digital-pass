import 'package:covid_control/base/util.dart';
import 'package:covid_control/view/base/ui/widget/checkbox.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CheckBoxForm extends StatefulWidget {
  final String title;

  const CheckBoxForm({Key key, this.title}) : super(key: key);

  @override
  _CheckBoxFormState createState() => _CheckBoxFormState();
}

class _CheckBoxFormState extends State<CheckBoxForm> {
  @override
  Widget build(BuildContext context) {
    return BaseForm(
      title: widget.title,
      child: Column(
        children: <Widget>[
          AppCheckBoxTile(title: 'Въехал в город'),
          AppCheckBoxTile(title: 'Повышенная температура'),
        ],
      ),
    );
  }
}

class TimeForm extends StatelessWidget {
  final String dateStart;
  final bool isViolation;

  const TimeForm({Key key, @required this.dateStart, this.isViolation = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    final start = dateFromString(dateStart);
    final df = DateFormat('HH:mm');
    final startTime = df.format(start);
    final currentTime = df.format(now);
    final difference = now.difference(start);

    final hour = difference.inSeconds > 0 ? difference.inHours : 0;
    final minute = difference.inSeconds > 0 ? difference.inMinutes % 60 : 0;
    return BaseForm(
        title: 'Время нахождения на улице',
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Opacity(
                opacity: 0.7,
                child: Icon(
                  Icons.access_time,
                  size: 40,
                  color: Theme.of(context).accentColor,
                )),
            SizedBox(width: 8),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    hour > 24 ? 'Более суток назад' : '$hour ч $minute мин',
                    style: Theme.of(context).textTheme.headline,
                  ),
                  Text(
                    'Вышел в $startTime - Текущее время $currentTime',
                    style: TextStyle(fontSize: 12),
                  ),
                  if (isViolation)
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: Text(
                        'Нарушено рекомендованное время пребывания на улице',
                        style: TextStyle(fontSize: 13, color: Colors.red[800]),
                      ),
                    ),
                ],
              ),
            )
          ],
        ));
  }
}

class ViolationTextForm extends StatelessWidget {
  final String title;
  final String subTitle;
  final bool isViolation;

  const ViolationTextForm({Key key, this.title, this.subTitle, this.isViolation = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var style = Theme.of(context).textTheme.body1;
    if (isViolation)
      style = style.apply(color: Colors.red[800]);
    return BaseForm(
      title: title,
      child: Text(
        subTitle ?? 'Не указано',
        style: style,
      ),
    );
  }
}


class TextForm extends StatelessWidget {
  final String title;
  final String subTitle;

  const TextForm({Key key, this.title, this.subTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseForm(
      title: title,
      child: Text(
        subTitle ?? 'Не указано',
        style: Theme.of(context).textTheme.body1,
      ),
    );
  }
}

class BaseForm extends StatelessWidget {
  final String title;
  final Widget child;

  const BaseForm({Key key, this.title, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          title,
          style: Theme.of(context).textTheme.caption,
        ),
        SizedBox(height: 8),
        child
      ],
    );
  }
}
