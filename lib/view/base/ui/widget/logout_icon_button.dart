import 'package:flutter/material.dart';

class LogoutIconButton extends StatelessWidget {
  final Function onExit;

  const LogoutIconButton({Key key, @required this.onExit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.exit_to_app),
      onPressed: () {
        _showAcceptDialog(context);
      },
    );
  }

  void _showAcceptDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (ctx) {
          return AlertDialog(
            title: Text('Выход'),
            content: Text('Выйти из аккаунта?'),
            actions: <Widget>[
              FlatButton(
                child: Text('Да'),
                onPressed: () {
                  Navigator.pop(ctx);
                  onExit?.call();
                },
              ),
              FlatButton(
                child: Text('Нет'),
                onPressed: () {
                  Navigator.pop(ctx);
                },
              ),
            ],
          );
        });
  }
}
