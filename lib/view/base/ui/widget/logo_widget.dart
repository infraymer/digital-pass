import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      alignment: Alignment.centerLeft,
      child: SvgPicture.asset('assets/logo.svg', height: 30),
    );
  }
}
