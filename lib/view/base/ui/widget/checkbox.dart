import 'package:flutter/material.dart';

class AppCheckBoxTile extends StatefulWidget {
  final String title;
  final Function(bool) onChanged;

  const AppCheckBoxTile({Key key, this.title, this.onChanged}) : super(key: key);

  @override
  _AppCheckBoxTileState createState() => _AppCheckBoxTileState();
}

class _AppCheckBoxTileState extends State<AppCheckBoxTile> {
  bool _value = false;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: _onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              AppCheckBox(
                value: _value,
              ),
              SizedBox(width: 12),
              Expanded(
                child: GestureDetector(
                    child: Text(widget.title,
                        style: Theme.of(context).textTheme.body1)),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onTap() {
    _value = !_value;
    setState(() {});
    widget.onChanged?.call(_value);
  }
}

class AppCheckBox extends StatelessWidget {
  final bool value;

  const AppCheckBox({Key key, @required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedOpacity(
          curve: Curves.decelerate,
          opacity: value ? 1 : 0,
          duration: Duration(milliseconds: 100),
          child: Icon(Icons.check_box, color: Theme.of(context).accentColor),
        ),
        Icon(Icons.check_box_outline_blank, color: Theme.of(context).accentColor),
      ],
    );
  }
}