import 'package:covid_control/base/util.dart';
import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/work.dart';
import 'package:covid_control/view/base/style/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BadStatusPersonWidget extends StatelessWidget {
  final Quarantine quarantine;

  const BadStatusPersonWidget(
      {Key key, this.quarantine})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final format = 'dd.MM.yyyy';
    final sickDate = dateFormat(quarantine.sickDate, format) ?? '[не указано]';
    final dateStart = dateFormat(quarantine.quarantineDateStart, format) ?? '[не указано]';
    final dateEnd = dateFormat(quarantine.quarantineDateEnd, format) ?? '[не указано]';

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
      width: double.infinity,
      color: AppColors.redBg,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Ограничения',
            style: TextStyle(
              fontSize: 18,
              color: Colors.red[800],
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 8),
          NegativeListTile(title: 'Находится на карантине с $dateStart по $dateEnd'),
          if (quarantine.isSick)
            NegativeListTile(title: 'Болен с $sickDate'),
          if (quarantine.isStationary)
            NegativeListTile(title: 'Находится на стационарном лечении')
        ],
      ),
    );
  }
}

class GoodStatusPersonWidget extends StatelessWidget {
  final List<Work> works;

  const GoodStatusPersonWidget({Key key, this.works}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
      width: double.infinity,
      color: AppColors.greenBg,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Разрешения',
            style: TextStyle(
                fontSize: 18,
                color: Colors.green[700],
                fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 8),
          PositiveListTile(
              title: 'Не найден в списке больных или карантинных.'),
          if (works != null && works.isNotEmpty) WorkListTile(works: works)
        ],
      ),
    );
  }
}

class NegativeListTile extends StatelessWidget {
  final String title;

  const NegativeListTile({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(Icons.clear, color: Colors.red[700], size: 24),
          SizedBox(width: 8),
          Expanded(
            child: Text(title, style: TextStyle(fontSize: 16)),
          )
        ],
      ),
    );
  }
}

class PositiveListTile extends StatelessWidget {
  final String title;

  const PositiveListTile({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(Icons.done, color: Colors.green[800], size: 24),
          SizedBox(width: 8),
          Expanded(
            child: Text(title, style: TextStyle(fontSize: 16)),
          )
        ],
      ),
    );
  }
}

class WorkListTile extends StatelessWidget {
  final List<Work> works;

  const WorkListTile({Key key, this.works}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Icon(Icons.done, color: Colors.green[800], size: 24),
              SizedBox(width: 8),
              Expanded(
                child: Text('Имеет разрешение на работу:',
                    style: TextStyle(fontSize: 16)),
              ),
            ],
          ),
          SizedBox(height: 16),
          Wrap(
            spacing: 8,
            runSpacing: 8,
            children: <Widget>[
              for (final work in works)
                WorkDayChip(
                  dayOfWeek: dayOfWeekToString(work.dayNumber),
                  period: _workPeriod(work),
                ),
            ],
          ),
          SizedBox(height: 16),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: 'Адрес: ',
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.bold,
              ),
              children: <TextSpan>[
                TextSpan(
                    text: works.first.address ?? 'Не указан',
                    style: TextStyle(fontWeight: FontWeight.w400)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  String _workPeriod(Work work) {
    final timeStart = dateFormat(work.timeStart, 'HH:mm');
    final timeEnd = dateFormat(work.timeEnd, 'HH:mm');
    return '$timeStart - $timeEnd';
  }
}

class WorkDayChip extends StatelessWidget {
  final String dayOfWeek;
  final String period;

  const WorkDayChip({Key key, this.dayOfWeek, this.period}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IntrinsicWidth(
      child: Container(
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(4),
              decoration: BoxDecoration(
                color: Colors.green[500],
                borderRadius: BorderRadius.circular(4),
              ),
              child: Text(dayOfWeek, style: TextStyle(color: Colors.white)),
            ),
            SizedBox(width: 4),
            Text(period, style: TextStyle(fontSize: 14)),
            SizedBox(width: 4),
          ],
        ),
      ),
    );
  }
}
