import 'package:flutter/material.dart';

class AppColors {
  static const Color captionText = Color(0x00000000);
  static const Color textFieldBg = Color(0xFFEDF2F6);
  static const Color clearButtonBg = Color(0xFFE1E8F0);
  static const Color clearButton = Color(0xFFB5BDCA);
  static const Color title = Color(0xFF6C737B);
  static const Color button = Color(0xFFCED4E0);
  static const Color redBg = Color(0xFFffe7e6);
  static const Color greenBg = Color(0xFFe5f5e4);
}