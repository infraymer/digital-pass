import 'package:flutter/material.dart';

class PersonPassportListItem extends StatelessWidget {
  final int index;
  final String name;
  final String passport;

  const PersonPassportListItem({Key key, this.name, this.passport, this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mainTextStyle =
    const TextStyle(fontWeight: FontWeight.bold, fontSize: 18);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          '$index.',
          style: mainTextStyle,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '$name',
                style: mainTextStyle,
              ),
              SizedBox(height: 8),
              Text(
                'Паспорт: $passport',
                style: Theme.of(context).textTheme.body2,
              ),
            ],
          ),
        )
      ],
    );
  }
}