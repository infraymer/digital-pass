import 'package:covid_control/view/base/style/colors.dart';
import 'package:flutter/material.dart';

class AppTabButton extends StatelessWidget {
  final bool isSelected;
  final String text;
  final Function onTap;

  const AppTabButton({Key key, this.isSelected, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final selectedTextColor = Colors.white;
    final accentColor = Theme.of(context).accentColor;
    final textFieldColor = AppColors.textFieldBg;

    return Material(
      color: isSelected ? accentColor : textFieldColor,
      child: InkWell(
        onTap: onTap,
        child: AnimatedContainer(
          duration: Duration(milliseconds: 150),
          curve: Curves.easeInOutQuad,
          padding: EdgeInsets.all(16),
          child: Center(
              child: AnimatedDefaultTextStyle(
                duration: Duration(milliseconds: 150),
                style: TextStyle(
                    color: isSelected ? selectedTextColor : Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14),
                child: Text(text),
              )),
        ),
      ),
    );
  }
}