import 'package:covid_control/base/util.dart';
import 'package:flutter/material.dart';

import 'app_tab_button.dart';

class TabWidget extends StatefulWidget {
  final Function(int) onSelected;
  final int selectedPosition;

  const TabWidget({Key key, this.onSelected, this.selectedPosition}) : super(key: key);
  @override
  _TabWidgetState createState() => _TabWidgetState();
}

class _TabWidgetState extends State<TabWidget> {
  final List<String> _tabs = ['По паспорту', 'По ФИО'];

  @override
  Widget build(BuildContext context) {
    return Row(
      children: mapIndexed(
        _tabs,
            (i, name) => Expanded(
          child: AppTabButton(
            isSelected: widget.selectedPosition == i,
            text: name,
            onTap: () {
              setState(() {});
              widget.onSelected?.call(i);
            },
          ),
        ),
      ).toList(),
    );
  }
}