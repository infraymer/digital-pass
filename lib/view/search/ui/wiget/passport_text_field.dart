import 'package:covid_control/view/base/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class PassportTextField extends StatefulWidget {
  final Function(String) onFilled;
  final Function(String, bool) onChanged;

  const PassportTextField({Key key, this.onFilled, this.onChanged})
      : super(key: key);

  @override
  _PassportTextFieldState createState() => _PassportTextFieldState();
}

class _PassportTextFieldState extends State<PassportTextField> {
  static const String _mask = '0000 000000';
  final _controller = MaskedTextController(mask: _mask);

  @override
  void initState() {
    super.initState();
    _controller.afterChange = (String previous, String next) {
      if (previous == next) return;
      // print('prev: $previous, next: $next');
      widget.onChanged
          ?.call(next.replaceAll(' ', ''), next.length == _mask.length);
      if (next.length == _mask.length)
        widget.onFilled?.call(next.replaceAll(' ', ''));
    };
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: Container(
        color: AppColors.textFieldBg,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                height: 70,
                child: Center(
                  child: TextField(
                      textAlign: TextAlign.center,
                      controller: _controller,
                      maxLines: 1,
                      autofocus: true,
                      keyboardType: TextInputType.number,
                      buildCounter: (BuildContext context,
                              {int currentLength,
                              int maxLength,
                              bool isFocused}) =>
                          null,
                      style:
                          TextStyle(fontSize: 32, fontWeight: FontWeight.w500),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      )),
                ),
              ),
            ),
            Material(
              color: AppColors.clearButtonBg,
              child: InkWell(
                onTap: () {
                  _controller.clear();
//                  _maskFormatter.formatEditUpdate(widget.controller.value, TextEditingValue.empty);
//                  widget.controller.value = TextEditingValue.empty;
                },
                child: Container(
                  width: 65,
                  height: 70,
//                  color: AppColors.clearButtonBg,
                  child:
                      Icon(Icons.clear, size: 36, color: AppColors.clearButton),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
