import 'package:flutter/material.dart';

class PersonListItem extends StatelessWidget {
  final int index;
  final String name;
  final String passport;
  final Function onTap;

  const PersonListItem({Key key, this.name, this.passport, this.index, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mainTextStyle =
    const TextStyle(fontWeight: FontWeight.bold, fontSize: 16);
    return Material(
      child: InkWell(
        onTap: onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '$index.',
                style: mainTextStyle,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$name',
                      style: mainTextStyle,
                    ),
                    SizedBox(height: 4),
                    Text(
                      'Паспорт: $passport',
                      style: Theme.of(context).textTheme.body1,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}