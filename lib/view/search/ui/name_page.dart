import 'package:covid_control/data/search/search_repository.dart';
import 'package:covid_control/domain/person/model/person.dart';
import 'package:covid_control/view/base/style/colors.dart';
import 'package:covid_control/view/person/ui/person_screen.dart';
import 'package:covid_control/view/search/ui/wiget/name_text_field.dart';
import 'package:covid_control/view/search/ui/wiget/person_list_item.dart';
import 'package:flutter/material.dart';

class NamePage extends StatefulWidget {
  const NamePage({Key key}) : super(key: key);

  @override
  _NamePageState createState() => _NamePageState();
}

class _NamePageState extends State<NamePage> {
  final _searchRepository = SearchRepository();

  Future<List<Person>> _futurePerson;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: Column(
        children: <Widget>[
          SizedBox(height: 16),
          _buildTextFieldHeader(),
          SizedBox(height: 16),
          NameTextField(
            onChanged: (value) {
              _futurePerson = _searchRepository.searchByName(value);
              setState(() {});
            },
          ),
          SizedBox(height: 16),
          Expanded(child: _searchResult()),
        ],
      ),
    );
  }

  Widget _searchResult() {
    return FutureBuilder<List<Person>>(
      future: _futurePerson,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return _buildProgress();
        }

        if (snapshot.hasData) {
          return _buildResults(snapshot.data);
        }

        if (snapshot.hasError) {
          return _buildError();
        }

        return SizedBox();
      },
    );
  }

  Widget _buildError() {
    return Center(
      child: Text(
        'Во время поиска возникла ошибка',
        style: TextStyle(color: Colors.red),
      ),
    );
  }

  Widget _buildResults(List<Person> list) {
    return ListView.builder(
      physics: BouncingScrollPhysics(),
      itemCount: list.length + 1,
      itemBuilder: (ctx, index) {
        if (index == 0) {
          return Column(
            children: <Widget>[
              _buildResultsHeader(list.length),
              SizedBox(height: 8),
            ],
          );
        }

        final data = list[index - 1];
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: PersonListItem(
            index: index + 1,
            name: data.name,
            passport: data.passport,
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (ctx) => PersonScreen(person: data),
              ));
            },
          ),
        );
      },
    );
  }

//  Widget _buildResults(List<Person> list) {
//    return Column(
//      children: <Widget>[
//        _buildResultsHeader(list.length),
//        SizedBox(height: 8),
//        for (final data in list)
//          Padding(
//            padding: const EdgeInsets.symmetric(vertical: 4),
//            child: PersonListItem(
//              index: list.indexOf(data) + 1,
//              name: data.name,
//              passport: data.passport,
//              onTap: () {
//                Navigator.of(context).push(MaterialPageRoute(
//                  builder: (ctx) => PersonScreen(person: data),
//                ));
//              },
//            ),
//          ),
//      ],
//    );
//  }

  Widget _buildTextFieldHeader() {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Введите ФИО',
            style: TextStyle(
                fontWeight: FontWeight.w500,
                color: AppColors.title,
                fontSize: 16),
          ),
          SizedBox(height: 4),
          Text(
            'Подсказка: Вводить в строгой последовательности Фамилия Имя Отчество, резделяя пробелами',
            style:
                TextStyle(color: Theme.of(context).accentColor, fontSize: 12),
          ),
        ],
      ),
    );
  }

  Widget _buildResultsHeader(int count) {
    final textStyle = TextStyle(
        fontWeight: FontWeight.bold, color: AppColors.title, fontSize: 14);
    return Container(
      width: double.infinity,
      child: Row(
        children: <Widget>[
          Text(
            'Результаты поиска',
            style: textStyle,
          ),
          SizedBox(width: 4),
          Text(
            '($count)',
            style: textStyle.apply(color: Theme.of(context).accentColor),
          ),
        ],
      ),
    );
  }

  Widget _buildProgress() {
    return Container(
      margin: EdgeInsets.all(24),
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
