import 'package:covid_control/data/login/login_repository.dart';
import 'package:covid_control/presentation/main/bloc.dart';
import 'package:covid_control/view/about/ui/about_screen.dart';
import 'package:covid_control/view/base/ui/widget/logo_widget.dart';
import 'package:covid_control/view/base/ui/widget/logout_icon_button.dart';
import 'package:covid_control/view/search/ui/name_page.dart';
import 'package:covid_control/view/search/ui/passport_page.dart';
import 'package:covid_control/view/search/ui/wiget/tab_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final LoginRepository _loginRepository = LoginRepository();
  int _currentTab = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                LogoWidget(),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.info_outline),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (ctx) => AboutScreen()));
                      },
                    ),
                    LogoutIconButton(
                      onExit: () {
                        _loginRepository.logOut();
                        BlocProvider.of<MainBloc>(context).add(LoggedOutMainEvent());
                      },
                    ),
                  ],
                )
              ],
            ),
            TabWidget(
              selectedPosition: _currentTab,
              onSelected: (i) {
                _currentTab = i;
                setState(() {});
              },
            ),
            Expanded(
              child: _currentTab == 0
                  ? PassportPage(
                      navigateToNameSearch: () {
                        _currentTab = 1;
                        setState(() {});
                      },
                    )
                  : NamePage(),
            ),
          ],
        ),
      ),
    );
  }
}
