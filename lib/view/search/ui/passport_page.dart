import 'package:covid_control/domain/person/model/person.dart';
import 'package:covid_control/domain/person/model/police_data_response.dart';
import 'package:covid_control/presentation/search_passport/search_passport_presenter.dart';
import 'package:covid_control/view/base/style/colors.dart';
import 'package:covid_control/view/base/ui/widget/buttons.dart';
import 'package:covid_control/view/base/ui/widget/checkbox.dart';
import 'package:covid_control/view/person/ui/person_screen.dart';
import 'package:covid_control/view/result_sent_data/ui/result_sent_data_screen.dart';
import 'package:covid_control/view/search/ui/wiget/passport_text_field.dart';
import 'package:covid_control/view/search/ui/wiget/person_list_item.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum ScreenState { main, form }

class PassportPage extends StatefulWidget {
  final Function navigateToNameSearch;

  const PassportPage({Key key, this.navigateToNameSearch}) : super(key: key);

  @override
  _PassportPageState createState() => _PassportPageState();
}

class _PassportPageState extends State<PassportPage> {
  final _presenter = SearchPassportPresenter();
  ScreenState _screenState = ScreenState.main;

  Future<List<Person>> _futureSearch;
  Future<PoliceDataResponse> _futurePoliceData;

  String _passportValue = '';
  bool _isTemperature = false;
  bool _isFilledPassport = false;

  @override
  void initState() {
    super.initState();
  }

  var key = Key('form');

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32),
      child: ListView(
        reverse: true,
        physics: BouncingScrollPhysics(),
        children: <Widget>[
//          SizedBox(height: 16),
//          _buildTextFieldHeader(),
//          SizedBox(height: 16),
//          PassportTextField(
//            onChanged: (String value, bool isFilled) {
//              _passportValue = value;
//              _isFilledPassport = isFilled;
//              setState(() {});
//            },
//          ),
//          if (_screenState == ScreenState.main && _isFilledPassport)
//            _buildFormButtons(),
//          SizedBox(height: 16),
//          if (_screenState == ScreenState.main) _searchResult(),
//          SizedBox(height: 8),
//          if (_screenState == ScreenState.form) _buildSendForm()
          if (_screenState == ScreenState.form)
            _buildSendForm(),
          SizedBox(height: 8),
          if (_screenState == ScreenState.main)
            _searchResult(),
          SizedBox(height: 16),
          if (_screenState == ScreenState.main && _isFilledPassport)
            _buildFormButtons(),
          PassportTextField(
            key: key,
            onChanged: (String value, bool isFilled) {
              _passportValue = value;
              _isFilledPassport = isFilled;
              setState(() {});
            },
          ),
          SizedBox(height: 16),
          _buildTextFieldHeader(),
          SizedBox(height: 16),
        ],
      ),
    );
  }

  void _search() {
    _futureSearch = _presenter.search(_passportValue);
    setState(() {});
  }

  Widget _searchResult() {
    return FutureBuilder<List<Person>>(
      future: _futureSearch?.then((value) {
        if (value.isNotEmpty) {
          _futureSearch = null;

          Navigator.of(context).push(MaterialPageRoute(
            builder: (ctx) => PersonScreen(person: value.first),
          ));
        }

        return value;
      }),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return _buildProgress();
        }

//        if (snapshot.hasData && snapshot.data.isNotEmpty) {
//          final data = snapshot.data.first;
//          return _buildData(data);
//        }

        if (snapshot.hasData && snapshot.data.isEmpty) {
          return _buildNoSearchResults();
        }

        return SizedBox();
      },
    );
  }

  Widget _buildData(Person data) {
    return Column(
      children: <Widget>[
        _buildResultsHeader(),
        SizedBox(height: 8),
        PersonListItem(
          name: data.name,
          passport: data.passport,
          index: 1,
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (ctx) => PersonScreen(person: data),
            ));
          },
        )
      ],
    );
  }

  Widget _buildTextFieldHeader() {
    return Container(
      width: double.infinity,
      child: Text(
        'Введите номер паспорта',
        style: TextStyle(
            fontWeight: FontWeight.w500, color: AppColors.title, fontSize: 16),
      ),
    );
  }

  Widget _buildResultsHeader() {
    return Container(
      width: double.infinity,
      child: Text(
        'Результаты поиска',
        style: TextStyle(
            fontWeight: FontWeight.bold, color: AppColors.title, fontSize: 16),
      ),
    );
  }

  Widget _buildNoSearchResults() {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Поиск не дал результатов.',
              style: Theme.of(context).textTheme.body1),
          SizedBox(height: 12),
          Material(
            child: InkWell(
              onTap: () {
                widget.navigateToNameSearch?.call();
              },
              child: IntrinsicWidth(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4),
                  child: Row(
                    children: <Widget>[
                      Text(
                        'Произведите поиск по ФИО',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 8),
                      Icon(Icons.arrow_forward,
                          color: AppColors.clearButton, size: 16)
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProgress() {
    return Container(
      margin: EdgeInsets.all(24),
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildSendForm() {
    return FutureBuilder<PoliceDataResponse>(
        future: _futurePoliceData?.then((value) {
          _futurePoliceData = null;
          _screenState = ScreenState.main;
          setState(() {});
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ResultSentDataScreen(
                passport: _passportValue,
                quarantine: value.quarantine,
                works: value.works,
              ),
            ),
          );

          return value;
        }),
        initialData: null,
        builder: (context, snapshot) {
          return Column(
            children: <Widget>[
              AppCheckBoxTile(
                title: 'Повышенная температура',
                onChanged: (value) {
                  _isTemperature = value;
                },
              ),
              SizedBox(height: 16),
              Row(
                children: <Widget>[
                  Expanded(
                      child: AppMainButton(
                    text: 'Отменить',
                    onPressed: () {
                      _screenState = ScreenState.main;
                      _futurePoliceData = null;
                      setState(() {});
                    },
                  )),
                  SizedBox(width: 16),
                  Expanded(
                    child: snapshot.connectionState == ConnectionState.waiting
                        ? Center(child: CircularProgressIndicator())
                        : AppAccentMainButton(
                            text: 'Отправить',
                            onPressed: _sendPoliceData,
                          ),
                  ),
                ],
              ),
              SizedBox(height: 16),
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.hasError)
                Text(
                  'Ошибка отправки данных',
                  style: TextStyle(color: Colors.red),
                ),
              if (snapshot.connectionState == ConnectionState.done &&
                  !snapshot.hasError)
                Text(
                  'Данные успешно отправлены!',
                  style: TextStyle(color: Colors.green),
                )
            ],
          );
        });
  }

  Widget _buildFormButtons() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: <Widget>[
          Expanded(
            child: AppAccentMainButton(
              text: 'Въехал в город',
              onPressed: () {
                _screenState = ScreenState.form;
                setState(() {});
              },
            ),
          ),
          SizedBox(width: 16),
          Expanded(
            child: AppAccentMainButton(
              color: Colors.green,
              text: 'Поиск в заявках',
              onPressed: () {
                _search();
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _sendPoliceData() async {
    _futurePoliceData = _presenter.sendPoliceData(_passportValue, _isTemperature, true);
    setState(() {});
  }
}
