import 'package:covid_control/presentation/login/bloc.dart';
import 'package:covid_control/base/extensions.dart';
import 'package:covid_control/view/base/ui/widget/buttons.dart';
import 'package:covid_control/view/login/ui/widget/login_text_field.dart';
import 'package:covid_control/view/login/ui/widget/password_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _loginController = TextEditingController();
  final _passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: BlocListener<LoginBloc, LoginState>(
            listener: (context, state) {
              if (state is ErrorLoginState) {
                _scaffoldKey.currentState.snackbar(state.message);
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 56),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Авторизация', style: Theme.of(context).textTheme.headline),
                  SizedBox(height: 32),
                  LoginTextField(controller: _loginController),
                  SizedBox(height: 16),
                  PasswordTextField(controller: _passController),
                  SizedBox(height: 16),
                  if (state is! LoadingLoginState)
                    Container(
                      width: double.infinity,
                      child: AppAccentMainButton(
                        text: 'Авторизоваться',
                        onPressed: () {
                          BlocProvider.of<LoginBloc>(context).add(
                            OnAuthClickedLoginEvent(
                              _loginController.text.trim(),
                              _passController.text.trim(),
                            )
                          );
                        },
                      ),
                    ),
                  if (state is LoadingLoginState)
                    Container(
                      margin: EdgeInsets.all(16),
                      child: Center(child: CircularProgressIndicator()),
                    ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
