import 'package:covid_control/view/base/style/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginTextField extends StatelessWidget {
  final TextEditingController controller;
  final Function(String) onChanged;

  const LoginTextField({Key key, this.controller, this.onChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(4),
      child: Container(
        color: AppColors.textFieldBg,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                height: 60,
                child: Center(
                  child: TextField(
                    textAlign: TextAlign.center,
                    controller: controller,
                    maxLines: 1,
                    autofocus: true,
                    textCapitalization: TextCapitalization.words,
                    onChanged: (value) {
                      onChanged?.call(value);
                    },
                    keyboardType: TextInputType.text,
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Введите логин'
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
