import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/work.dart';
import 'package:covid_control/view/base/ui/widget/buttons.dart';
import 'package:covid_control/view/base/ui/widget/logo_widget.dart';
import 'package:covid_control/view/base/ui/widget/permissions_widget.dart';
import 'package:covid_control/view/base/ui/widget/status_person_widget.dart';
import 'package:flutter/material.dart';
import 'package:string_mask/string_mask.dart';

class ResultSentDataScreen extends StatelessWidget {
  final String passport;
  final Quarantine quarantine;
  final List<Work> works;

  const ResultSentDataScreen({Key key, this.passport, this.works, this.quarantine})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            LogoWidget(),
            Column(
              children: <Widget>[
                _buildTitle(),
                SizedBox(height: 16),
                Divider(indent: 32, endIndent: 32),
                SizedBox(height: 16),
                PermissionsWidget(quarantine: quarantine, works: works),
              ],
            ),
            Container(
              margin: EdgeInsets.all(32),
              width: double.infinity,
              child: AppAccentMainButton(
                text: 'Вернуться назад',
                onPressed: () => Navigator.pop(context),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildTitle() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 32),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          text: 'Номер паспорта ',
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.w500),
          children: <TextSpan>[
            TextSpan(
                text: StringMask('0000 000000').apply(passport),
                style: TextStyle(fontWeight: FontWeight.w800)),
            TextSpan(text: '\nдобавлен в базу въехавших в город.'),
          ],
        ),
      ),
    );
  }
}
