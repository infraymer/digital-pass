import 'package:covid_control/app.dart';
import 'package:covid_control/base/config.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp(buildParam: BuildParam.prod));
}