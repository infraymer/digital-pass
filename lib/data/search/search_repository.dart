import 'package:covid_control/domain/person/model/person.dart';
import 'package:covid_control/remote/base/api.dart';

class SearchRepository {
  Future<List<Person>> searchByPassport(String number) async {
    final response = await dio.post(
      '/citizen/search',
      data: {
        'passport_number': number,
      },
    );
    final usersJson = response.data['users'] as List;
    final data = usersJson.map((e) => Person.fromJson(e)).toList();
    return data;
  }

  Future<List<Person>> searchByName(String query) async {
    if (query.isEmpty) {
      return [];
    }

    final response = await dio.post(
      '/citizen/search',
      data: {
        'last_name': query,
      },
    );
    final usersJson = response.data['users'] as List;
    final data = usersJson.map((e) => Person.fromJson(e)).toList();
    return data;
  }
}
