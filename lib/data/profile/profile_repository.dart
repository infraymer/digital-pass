import 'package:covid_control/remote/base/api.dart';

class ProfileRepository {

  Future<void> getProfile() async {
    final response = await dio.post('/auth/me');
    print(response);
  }
}