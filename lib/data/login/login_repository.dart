import 'package:covid_control/domain/login/model/login_data.dart';
import 'package:covid_control/app.dart';
import 'package:covid_control/remote/base/api.dart';

class LoginRepository {
  Future<bool> isLogged() async {
    final token = await appStorage.getToken();
    return token.isNotEmpty;
  }

  Future<void> login(LoginData loginData) async {
    final response = await dio.post(
      '/auth/login',
      queryParameters: {
        'email': loginData.email,
        'password': loginData.password,
      },
    );
    if (response.statusCode == 200) {
      final token = response.data['access_token'];
      appStorage.setToken(token);
      appStorage.setLoginData(loginData);
    } else {
      throw('Error');
    }
  }

  Future<void> logOut() async {
    return await appStorage.setToken(null);
  }
}
