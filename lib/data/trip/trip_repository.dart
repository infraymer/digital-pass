import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/trip.dart';
import 'package:covid_control/domain/trip/model/trip_data.dart';
import 'package:covid_control/domain/trip/model/work.dart';
import 'package:covid_control/remote/base/api.dart';

class TripRepository {

  Future<TripData> getTrips(int userId) async {
    final response = await dio.get('/citizen/$userId/trips');
    final tripsJson = response.data['trips']['data'] as List;
    final quarantine = response.data['quarantine'] != null ? Quarantine.fromJson(response.data['quarantine']) : null;
    final work = response.data['work'] as List;
    final data = tripsJson.map((e) => Trip.fromJson(e)).toList();
    final works = work?.map((e) => Work.fromJson(e))?.toList();
    return TripData(data, works, quarantine);
  }
}