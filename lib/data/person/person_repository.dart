import 'package:covid_control/domain/person/model/police_data.dart';
import 'package:covid_control/domain/person/model/police_data_response.dart';
import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/work.dart';
import 'package:covid_control/remote/base/api.dart';

class PersonRepository {
  Future<PoliceDataResponse> setPoliceData(PoliceData data) async {
    final response = await dio.post(
      '/citizen/setPoliceData',
      data: {
        'passport_number': data.passport,
        'point_lat': data.lat,
        'point_long': data.lon,
        'is_high_temperature': data.isTemperature,
        'is_entering': data.isEntering,
      },
    );
    if (response.statusCode != 200) {
      throw ('Error');
    }
    final quarantine = response.data['quarantine'] != null
        ? Quarantine.fromJson(response.data['quarantine'])
        : null;

    final work = response.data['work'] as List;
    final works = work?.map((e) => Work.fromJson(e))?.toList();
    return PoliceDataResponse(works, quarantine);
  }
}