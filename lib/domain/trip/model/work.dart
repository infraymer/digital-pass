import 'package:json_annotation/json_annotation.dart';

part 'work.g.dart';

@JsonSerializable()
class Work {
  @JsonKey(name: 'point_end')
  final String address;
  @JsonKey(name: 'time_start')
  final String timeStart;
  @JsonKey(name: 'time_end')
  final String timeEnd;
  @JsonKey(name: 'day_number')
  final int dayNumber;
  @JsonKey(name: 'company_id')
  final int companyId;
  final Company company;


  Work(this.address, this.timeStart, this.timeEnd, this.dayNumber, this.companyId, this.company);

  factory Work.fromJson(Map<String, dynamic> json) => _$WorkFromJson(json);
  Map<String, dynamic> toJson() => _$WorkToJson(this);
}

@JsonSerializable()
class Company {
  final int id;
  final String title;
  final String inn;
  final String phone;

  Company(this.id, this.title, this.inn, this.phone);

  factory Company.fromJson(Map<String, dynamic> json) => _$CompanyFromJson(json);
  Map<String, dynamic> toJson() => _$CompanyToJson(this);
}