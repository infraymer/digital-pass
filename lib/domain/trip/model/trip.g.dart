// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'trip.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Trip _$TripFromJson(Map<String, dynamic> json) {
  return Trip(
    json['id'] as int,
    json['user_id'] as int,
    json['reason_id'] as int,
    json['company_id'] as int,
    json['point_start'] as String,
    json['point_start_lat'] as String,
    json['point_start_long'] as String,
    json['point_end'] as String,
    json['point_end_lat'] as String,
    json['point_end_long'] as String,
    json['is_hight_temperature'] as bool,
    json['is_entering'] as bool,
    json['destination_name'] as String,
    json['time_start'] as String,
    json['time_end'] as String,
    json['day_number'] as int,
    json['created_at'] as String,
    json['company'] as Map<String, dynamic>,
    json['reason'] == null
        ? null
        : Reason.fromJson(json['reason'] as Map<String, dynamic>),
    json['time_violation'] as bool,
  );
}

Map<String, dynamic> _$TripToJson(Trip instance) => <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'reason_id': instance.reasonId,
      'company_id': instance.companyId,
      'point_start': instance.pointStart,
      'point_start_lat': instance.pointStartLat,
      'point_start_long': instance.pointStartLon,
      'point_end': instance.pointEnd,
      'point_end_lat': instance.pointEndLat,
      'point_end_long': instance.pointEndLon,
      'is_hight_temperature': instance.isHighTemperature,
      'is_entering': instance.isEntering,
      'destination_name': instance.destinationName,
      'time_start': instance.timeStart,
      'time_end': instance.timeEnd,
      'day_number': instance.dayNumber,
      'created_at': instance.createdAt,
      'company': instance.company,
      'reason': instance.reason,
      'time_violation': instance.isTimeViolation,
    };

Reason _$ReasonFromJson(Map<String, dynamic> json) {
  return Reason(
    json['id'] as int,
    json['title'] as String,
    json['need_finish_point'] as bool,
  );
}

Map<String, dynamic> _$ReasonToJson(Reason instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'need_finish_point': instance.needFinishPoint,
    };
