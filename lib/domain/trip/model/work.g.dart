// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'work.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Work _$WorkFromJson(Map<String, dynamic> json) {
  return Work(
    json['point_end'] as String,
    json['time_start'] as String,
    json['time_end'] as String,
    json['day_number'] as int,
    json['company_id'] as int,
    json['company'] == null
        ? null
        : Company.fromJson(json['company'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$WorkToJson(Work instance) => <String, dynamic>{
      'point_end': instance.address,
      'time_start': instance.timeStart,
      'time_end': instance.timeEnd,
      'day_number': instance.dayNumber,
      'company_id': instance.companyId,
      'company': instance.company,
    };

Company _$CompanyFromJson(Map<String, dynamic> json) {
  return Company(
    json['id'] as int,
    json['title'] as String,
    json['inn'] as String,
    json['phone'] as String,
  );
}

Map<String, dynamic> _$CompanyToJson(Company instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'inn': instance.inn,
      'phone': instance.phone,
    };
