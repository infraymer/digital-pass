import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/trip.dart';
import 'package:covid_control/domain/trip/model/work.dart';

class TripData {
  final List<Trip> trips;
  final List<Work> works;
  final Quarantine quarantine;

  TripData(this.trips, this.works, this.quarantine);
}