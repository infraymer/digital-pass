import 'package:json_annotation/json_annotation.dart';

part 'trip.g.dart';

@JsonSerializable()
class Trip {
  final int id;
  @JsonKey(name: 'user_id')
  final int userId;
  @JsonKey(name: 'reason_id')
  final int reasonId;
  @JsonKey(name: 'company_id')
  final int companyId;
  @JsonKey(name: 'point_start')
  final String pointStart;
  @JsonKey(name: 'point_start_lat')
  final String pointStartLat;
  @JsonKey(name: 'point_start_long')
  final String pointStartLon;
  @JsonKey(name: 'point_end')
  final String pointEnd;
  @JsonKey(name: 'point_end_lat')
  final String pointEndLat;
  @JsonKey(name: 'point_end_long')
  final String pointEndLon;
  @JsonKey(name: 'is_hight_temperature')
  final bool isHighTemperature;
  @JsonKey(name: 'is_entering')
  final bool isEntering;
  @JsonKey(name: 'destination_name')
  final String destinationName;
  @JsonKey(name: 'time_start')
  final String timeStart;
  @JsonKey(name: 'time_end')
  final String timeEnd;
  @JsonKey(name: 'day_number')
  final int dayNumber;
  @JsonKey(name: 'created_at')
  final String createdAt;
  final Map company;
  final Reason reason;
  @JsonKey(name: 'time_violation')
  final bool isTimeViolation;

  Trip(this.id, this.userId, this.reasonId, this.companyId, this.pointStart, this.pointStartLat, this.pointStartLon, this.pointEnd, this.pointEndLat, this.pointEndLon, this.isHighTemperature, this.isEntering, this.destinationName, this.timeStart, this.timeEnd, this.dayNumber, this.createdAt, this.company, this.reason, this.isTimeViolation);

  factory Trip.fromJson(Map<String, dynamic> json) => _$TripFromJson(json);
  Map<String, dynamic> toJson() => _$TripToJson(this);
}

@JsonSerializable()
class Reason {
  final int id;
  final String title;
  @JsonKey(name: 'need_finish_point')
  final bool needFinishPoint;

  Reason(this.id, this.title, this.needFinishPoint);

  factory Reason.fromJson(Map<String, dynamic> json) => _$ReasonFromJson(json);
  Map<String, dynamic> toJson() => _$ReasonToJson(this);
}