// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quarantine.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Quarantine _$QuarantineFromJson(Map<String, dynamic> json) {
  return Quarantine(
    json['id'] as int,
    json['first_name'] as String,
    json['last_name'] as String,
    json['patronymic'] as String,
    json['passport_number'] as String,
    json['quarantine_date_start'] as String,
    json['quarantine_date_end'] as String,
    json['is_sick'] as bool,
    json['sick_date'] as String,
    json['diagnosis'] as String,
    json['is_stationary'] as bool,
  );
}

Map<String, dynamic> _$QuarantineToJson(Quarantine instance) =>
    <String, dynamic>{
      'id': instance.id,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'patronymic': instance.secondName,
      'passport_number': instance.passport,
      'quarantine_date_start': instance.quarantineDateStart,
      'quarantine_date_end': instance.quarantineDateEnd,
      'is_sick': instance.isSick,
      'sick_date': instance.sickDate,
      'diagnosis': instance.diagnosis,
      'is_stationary': instance.isStationary,
    };
