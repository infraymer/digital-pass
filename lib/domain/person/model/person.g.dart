// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'person.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Person _$PersonFromJson(Map<String, dynamic> json) {
  return Person(
    json['id'] as int,
    json['first_name'] as String,
    json['patronymic'] as String,
    json['last_name'] as String,
    json['passport_number'] as String,
  );
}

Map<String, dynamic> _$PersonToJson(Person instance) => <String, dynamic>{
      'id': instance.id,
      'first_name': instance.firstName,
      'patronymic': instance.secondName,
      'last_name': instance.lastName,
      'passport_number': instance.passport,
    };
