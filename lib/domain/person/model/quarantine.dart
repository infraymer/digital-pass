import 'package:json_annotation/json_annotation.dart';

part 'quarantine.g.dart';

@JsonSerializable()
class Quarantine {
  final int id;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'patronymic')
  final String secondName;
  @JsonKey(name: 'passport_number')
  final String passport;
  @JsonKey(name: 'quarantine_date_start')
  final String quarantineDateStart;
  @JsonKey(name: 'quarantine_date_end')
  final String quarantineDateEnd;
  @JsonKey(name: 'is_sick')
  final bool isSick;
  @JsonKey(name: 'sick_date')
  final String sickDate;
  @JsonKey(name: 'diagnosis')
  final String diagnosis;
  @JsonKey(name: 'is_stationary')
  final bool isStationary;

  Quarantine(this.id, this.firstName, this.lastName, this.secondName, this.passport, this.quarantineDateStart, this.quarantineDateEnd, this.isSick, this.sickDate, this.diagnosis, this.isStationary);

  factory Quarantine.fromJson(Map<String, dynamic> json) => _$QuarantineFromJson(json);
  Map<String, dynamic> toJson() => _$QuarantineToJson(this);
}