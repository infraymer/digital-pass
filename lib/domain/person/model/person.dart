import 'package:json_annotation/json_annotation.dart';

part 'person.g.dart';

@JsonSerializable()
class Person {
  final int id;
  @JsonKey(name: 'first_name')
  final String firstName;
  @JsonKey(name: 'patronymic')
  final String secondName;
  @JsonKey(name: 'last_name')
  final String lastName;
  @JsonKey(name: 'passport_number')
  final String passport;

  String get name => '${lastName ?? ''} ${firstName ?? ''} ${secondName ?? ''}'.trim();

  Person(this.id, this.firstName, this.secondName, this.lastName, this.passport);

  factory Person.fromJson(Map<String, dynamic> json) => _$PersonFromJson(json);
  Map<String, dynamic> toJson() => _$PersonToJson(this);
}