import 'package:covid_control/domain/person/model/quarantine.dart';
import 'package:covid_control/domain/trip/model/work.dart';

class PoliceDataResponse {
  final List<Work> works;
  final Quarantine quarantine;

  PoliceDataResponse(this.works, this.quarantine);
}