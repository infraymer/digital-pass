class PoliceData {
  final String passport;
  final String lat;
  final String lon;
  final bool isTemperature;
  final bool isEntering;

  PoliceData(this.passport, this.lat, this.lon, this.isTemperature, this.isEntering);
}