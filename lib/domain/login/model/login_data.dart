import 'package:flutter/foundation.dart';

class LoginData {
  final String email;
  final String password;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const LoginData({
    @required this.email,
    @required this.password,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LoginData &&
          runtimeType == other.runtimeType &&
          email == other.email &&
          password == other.password);

  @override
  int get hashCode => email.hashCode ^ password.hashCode;

  @override
  String toString() {
    return 'LoginData{' + ' email: $email,' + ' password: $password,' + '}';
  }

  LoginData copyWith({
    String email,
    String password,
  }) {
    return new LoginData(
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'email': this.email,
      'password': this.password,
    };
  }

  factory LoginData.fromMap(Map<String, dynamic> map) {
    return new LoginData(
      email: map['email'] as String,
      password: map['password'] as String,
    );
  }

//</editor-fold>
}