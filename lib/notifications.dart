import 'package:rxdart/rxdart.dart';

final PublishSubject<bool> unauthorizedSubject = PublishSubject();