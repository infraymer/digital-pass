import 'package:covid_control/app.dart';
import 'package:covid_control/notifications.dart';
import 'package:dio/dio.dart';

final BaseOptions _baseOptions = BaseOptions(
  baseUrl: appConfig.apiBaseUrl,
);

final Dio dio = new Dio(_baseOptions)
  ..interceptors.add(
    InterceptorsWrapper(
      onRequest: (RequestOptions options) async {
        final token = await appStorage.getToken();
        if (token.isNotEmpty) {
          options.headers['Authorization'] = 'Bearer $token';
        }
        return options;
      },
      onError: (DioError error) async {
        final response = error.response;

        if (response == null || response.statusCode == null) {
          return error;
        }

        if (response.statusCode == 401) {
          final token = await appStorage.getToken();

          if (token.isEmpty) {
            return response;
          }

          try {
            final authData = await appStorage.getLoginData();
            final nextResponse = await dio.post(
              '/auth/login',
              queryParameters: {
                'email': authData.email,
                'password': authData.password,
              },
            );

            if (nextResponse.statusCode != 200) {
              unauthorizedSubject.add(true);
              await appStorage.setToken(null);
            }

            final newToken = nextResponse.data['access_token'] as String;
            await appStorage.setToken(newToken);

            final RequestOptions options = response.request;
            options.headers['Authorization'] = 'Bearer $newToken';
            final repeatResponse = await Dio(_baseOptions).request(options.path, options: options);

            return repeatResponse;
          } catch (e) {
            unauthorizedSubject.add(true);
            await appStorage.setToken(null);
          }
        }

        return error;
      },
    ),
  )
  ..interceptors.add(
    InterceptorsWrapper(onResponse: (Response response) async {
      if (response.statusCode == 401) {
        final token = await appStorage.getToken();

        if (token.isEmpty) {
          return response;
        }

        final responseRefresh =
            await dio.post('/auth/refresh', queryParameters: {'token': token});
        final newToken = responseRefresh.data['access_token'] as String;
        await appStorage.setToken(newToken);
        final RequestOptions options = responseRefresh.request;
        options.queryParameters['token'] = newToken;
        final nextResponse = await dio.request(options.path, options: options);

        if (nextResponse.statusCode == 401) {
          unauthorizedSubject.add(true);
          appStorage.setToken(null);
        }

        return nextResponse;
      }

      return response;
    }),
  )
  ..interceptors.add(
    LogInterceptor(
      requestBody: true,
      responseBody: true,
    ),
  );
