import 'package:covid_control/presentation/main/bloc.dart';
import 'package:covid_control/presentation/main/main_bloc.dart';
import 'package:covid_control/view/login/ui/login_screen.dart';
import 'package:covid_control/view/search/ui/search_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'base/config.dart';
import 'cache/base/app_storage.dart';
import 'presentation/login/bloc.dart';

AppConfig appConfig;
final appStorage = AppStorage();

class MyApp extends StatelessWidget {

  MyApp({Key key, BuildParam buildParam}) : super(key: key) {
    appConfig = AppConfig(buildParam: buildParam);
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<MainBloc>(
          create: (context) => MainBloc()..add(InitMainEvent()),
        ),
        BlocProvider<LoginBloc>(
          create: (context) => LoginBloc(
            mainBloc: BlocProvider.of<MainBloc>(context),
          ),
        )
      ],
      child: MaterialApp(
        title: 'Covid',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          accentColor: Color(0xFF013189),
        ),
        home: BlocBuilder<MainBloc, MainState>(
          builder: (context, state) {
            if (state is LoginMainState) {
              return LoginScreen();
            }

            if (state is HomeMainState) {
              return SearchScreen();
            }

            return SizedBox();
          },
        ),
      ),
    );
  }
}
