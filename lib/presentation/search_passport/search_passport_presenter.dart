import 'package:covid_control/data/person/person_repository.dart';
import 'package:covid_control/data/search/search_repository.dart';
import 'package:covid_control/domain/person/model/person.dart';
import 'package:covid_control/domain/person/model/police_data.dart';
import 'package:covid_control/domain/person/model/police_data_response.dart';
import 'package:geolocation/geolocation.dart';

class SearchPassportPresenter {

  final SearchRepository _searchRepository = SearchRepository();
  final PersonRepository _personRepository = PersonRepository();

  Future<List<Person>> search(String passport) async {
    final list = await _searchRepository.searchByPassport(passport);
    if (list.isNotEmpty) {
      await sendPoliceData(passport, false, false);
    }
    return list;
  }

  Future<PoliceDataResponse> sendPoliceData(String passport, bool isTemp, bool isEntering) async {
    LocationResult result = await Geolocation.lastKnownLocation();
    String lat = '0.0';
    String lon = '0.0';

    if (result.isSuccessful) {
      lat = result.location.latitude.toString();
      lon = result.location.longitude.toString();
    }

    final data = PoliceData(passport, lat, lon, false, false);
    return await _personRepository.setPoliceData(data);
  }
}