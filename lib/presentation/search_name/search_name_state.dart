import 'package:equatable/equatable.dart';

abstract class SearchNameState extends Equatable {
  const SearchNameState();
}

class InitialSearchNameState extends SearchNameState {
  @override
  List<Object> get props => [];
}
