import 'package:equatable/equatable.dart';

abstract class SearchNameEvent extends Equatable {
  const SearchNameEvent();
}
