import 'package:equatable/equatable.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
  @override
  List<Object> get props => [];
}

class OnAuthClickedLoginEvent extends LoginEvent {
  final String login;
  final String password;

  OnAuthClickedLoginEvent(this.login, this.password);

  @override
  List<Object> get props => [login, password];
}
