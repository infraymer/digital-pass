import 'package:equatable/equatable.dart';

abstract class LoginState extends Equatable {
  const LoginState();
  @override
  List<Object> get props => [];
}

class InitialLoginState extends LoginState {}
class LoadingLoginState extends LoginState {}
class SuccessLoginState extends LoginState {}
class ErrorLoginState extends LoginState {
  final String message;

  ErrorLoginState(this.message);
  @override
  List<Object> get props => [message];
}
