import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:covid_control/data/login/login_repository.dart';
import 'package:covid_control/domain/login/model/login_data.dart';
import 'package:covid_control/presentation/main/bloc.dart';
import './bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final MainBloc mainBloc;

  final LoginRepository _loginRepository = LoginRepository();

  LoginBloc({this.mainBloc});

  @override
  LoginState get initialState => InitialLoginState();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is OnAuthClickedLoginEvent) {
      try {
        yield LoadingLoginState();
        await _loginRepository.login(LoginData(email: event.login, password: event.password));
        yield SuccessLoginState();
        mainBloc.add(LoggedInMainEvent());
      } catch (e) {
        yield ErrorLoginState('Ошибка авторизации');
      }
    }
  }
}
