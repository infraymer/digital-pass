import 'package:equatable/equatable.dart';

abstract class MainState extends Equatable {
  const MainState();
  @override
  List<Object> get props => [];
}

class IdleMainState extends MainState {}
class LoginMainState extends MainState {}
class HomeMainState extends MainState {}
