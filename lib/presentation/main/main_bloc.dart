import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid_control/data/login/login_repository.dart';
import 'package:covid_control/notifications.dart';

import './bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  LoginRepository _loginRepository = LoginRepository();
  StreamSubscription _subscription;

  MainBloc() {
    _subscription = unauthorizedSubject.listen((value) {
      add(LoggedOutMainEvent());
    });
  }

  @override
  Future<void> close() {
    _subscription.cancel();
    return super.close();
  }

  @override
  MainState get initialState => IdleMainState();

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    if (event is LoggedInMainEvent) {
      yield HomeMainState();
    } else if (event is LoggedOutMainEvent) {
      yield LoginMainState();
    } else if (event is InitMainEvent) {
      bool isLogin = await _loginRepository.isLogged();
      yield isLogin ? HomeMainState() : LoginMainState();
    }
  }
}
