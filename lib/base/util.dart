import 'package:intl/intl.dart';

Iterable<E> mapIndexed<E, T>(
    Iterable<T> items, E Function(int index, T item) f) sync* {
  var index = 0;

  for (final item in items) {
    yield f(index, item);
    index = index + 1;
  }
}

DateTime dateFromString(String date) {
  return DateFormat('yyyy-MM-dd HH:mm:ss').parse(date);
}

String dateFormat(String date, String format) {
  if (date == null) return null;
  final d = dateFromString(date);
  return DateFormat(format).format(d);
}

String dayOfWeekToString(int dayNumber) {
  var str = '??';
  try {
    final days = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'];
    str = days[dayNumber];
  } catch(e) {}
  return str;
}