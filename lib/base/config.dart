enum BuildParam {
  prod, stage
}

const Map<BuildParam, String> _apiBaseUrlMap = {
  BuildParam.prod: 'https://controldoma.admhmao.ru/api',
  BuildParam.stage: 'https://controltest.admhmao.ru/api',
};

class AppConfig {
  String apiBaseUrl = 'https://controldoma.admhmao.ru/api';
  BuildParam buildParam;


  AppConfig({BuildParam buildParam = BuildParam.stage}) {
    this.buildParam = buildParam;
    apiBaseUrl = _apiBaseUrlMap[buildParam];
  }
}