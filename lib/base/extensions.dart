import 'package:flutter/material.dart';

extension ScaffoldStateExt on ScaffoldState {
  snackbar(String message) => this.showSnackBar(SnackBar(
    content: Text(message),
    duration: Duration(seconds: 3),
  ));
}